new Typed("#typed", {
	strings: ["Welcome!"],
	typeSpeed: 120,
	startDelay: 80,
	loop: true,
	cursorChar: "_",
});

const navbar = document.querySelectorAll("nav ul li a");
const toggle = document.getElementById("toggle");
const asideContainer = document.querySelector(".aside-container");

toggle.addEventListener("click", () => {
	asideContainer.classList.toggle("display-none");
	toggle.querySelectorAll("i").forEach((icon) => {
		icon.classList.toggle("display-none");
	});
});

window.addEventListener("load", () => {
	navbar[0].classList.add("active");
});

const changeActive = (currentItem) => {
	navbar.forEach((nav) => {
		if (nav === currentItem) {
			nav.classList.add("active");
		} else {
			nav.classList.remove("active");
		}
	});
};

navbar.forEach((nav) => {
	nav.addEventListener("click", () => {
		changeActive(nav);
	});
});
